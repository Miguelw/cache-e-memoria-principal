Projeto: SIMULADOR DE MEMÓRIA CACHE  
Autor: Welligton Miguel
Matrícula: 2015031796


Descrição: 

O projeto consiste em implementar uma ferramenta que simule o comportamento interno de uma cache L1 e da memória principal. O simulador recebe como entrada uma sequencia de comandos que podem ser de leitura ou escrita e o endereço solicitado. A cache simulada deve verificar a presença ou não do bloco contendo o dado solicitado (acarretando em um Hit ou Miss). O simulador deve ser configurável no que diz respeito aos aspectos de projeto de cache discutidos em sala: Mapeamento, Politica de Substituição e Política de Escrita.

Plataforma:

	- WINDOWS.

Execução:

O projeto trás consigo um arquivo executável "HierarquiaMemoria.jar" a qual deve ser executado.

1° Seleção de arquivo:

	- Ao executar o projeto basta clicar na aba "configuração" e posteriormente no botão "Selecionar" e com o auxilio do seletor de arquivo selecionar o arquivo (.txt) com as configurações do simulador e por fim clicar no botão "Carregar Configurações".

2° Análise de arquivo:

	- Após efetuado o passo 1° o programa permite as operações de leitura e escrita

	2.1° Leitura

		- O programa simula a busca na memória principal do endereço informado e o aloca na memporia cache, apresentando se houve um "miss ou hit".

	2.2° Escrita

		- É efetuado o passo 2.1° no entanto é atribuído no campo de conteúdo o valor informado para atribuição.

3° Geração de Log:
	
	- Após realizado a simulação o sistema desbloqueia a opção "Visualizar Estruturas", a mesma abre através do programa bloco de notas "Windows" a estrutura atual da memória cache e principal

	3.1 Memória Cache:

	- "Linha - Bloco - Endereco - Conteúdo"

	3.1 Memória Principal:

	- "Bloco - Endereco - Conteúdo"


4° Realizar nova simulação:

	- Para realização de nova simulação é necessário efetuar o passo 1° integralmente ou se preferir refinar a busca apenas clicar no botão "alterar" referente ao campo que deseja atualizar.

5° Códigos:

	- Para visualização do códio acessar dentro da pasta do projeto: ...\cache-e-memoria-principal\src

6° executável:

	- Para utilização do executável acessar a pasta: ...\HierarquiaMemoria\dist

7° JAVADOC:

	- Para acessar o JAVA DOC acessar a pasta ...\HierarquiaMemoria\dist\Javadoc\index.html