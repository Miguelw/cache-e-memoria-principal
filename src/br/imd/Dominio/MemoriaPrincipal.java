package br.imd.Dominio;

import br.imd.Controle.Bloco;

/**
 *
 * @author Miguel
 * @version 1.0
 */
public class MemoriaPrincipal {

    private Bloco[] blocos;

    /**
     * Contrutor da Memória Principal
     *
     * @param qtdPalavras qunatidade de palavras que a memória conterá
     * @param qtdBlocos quantidade de blocos da memória
     */
    public MemoriaPrincipal(int qtdPalavras, int qtdBlocos) {
        this.blocos = new Bloco[qtdBlocos];
        inicializarBlocos(qtdPalavras);
        inicializarEnderecoBloco();
    }

    /**
     * Get quantidade de blocos
     *
     * @return int quantidade de blocos
     */
    public int getQtdBloco() {
        return blocos.length;
    }

    /**
     * Get quantidade de palavras
     *
     * @return int com a quantidade de palavras em cada bloco
     */
    public int getQtdPalavras() {
        return blocos[0].getQtdPalavras();
    }

    /**
     * Get Bloco
     *
     * @return blocos contidos na memória principal
     */
    public Bloco[] getBlocos() {
        return blocos;
    }

    /**
     * Set Blocos
     *
     * @param blocos blocos a serem inseridos na memória
     */
    public void setBlocos(Bloco[] blocos) {
        this.blocos = blocos;
    }

    /**
     * Função para identificar o bloco da memória
     *
     * @param endereco endereço conde o bloco se localiza
     * @return Bloco desejado
     * @throws ArrayIndexOutOfBoundsException Exceção de posicionamento de vetor
     */
    public Bloco pegarBloco(int endereco) throws ArrayIndexOutOfBoundsException {
        return blocos[endereco / getQtdPalavras()];
    }

    /**
     * Função para inicializar os blocos da memória
     *
     * @param qtdPalavras quantidade de palavras que cada bloco conterá
     */
    private void inicializarBlocos(int qtdPalavras) {
        for (int cont = 0; cont < blocos.length; cont++) {
            blocos[cont] = new Bloco(qtdPalavras);
        }
    }

    /**
     * Função para inicializar os endereços de cada bloco
     */
    private void inicializarEnderecoBloco() {
        int valorEnderecoBloco = 0;
        int valorEnderecoPalavra = 0;
        for (Bloco bloco : blocos) {
            for (int contPalavra = 0; contPalavra < bloco.getQtdPalavras(); contPalavra++) {
                bloco.getPalavras()[contPalavra].setValorEndereco(valorEnderecoPalavra);
                bloco.inserirEnderecoBloco(contPalavra, valorEnderecoBloco);
                valorEnderecoPalavra++;
            }
            valorEnderecoBloco++;
        }
    }
}
