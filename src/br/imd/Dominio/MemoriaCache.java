package br.imd.Dominio;

import br.imd.Aplicacao.Fronteira;
import br.imd.Controle.Bloco;
import br.imd.Controle.Palavra;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Miguel
 * @version 1.0
 */
public class MemoriaCache {

    private final ArrayList<Integer> listaRecentementeAcessada;
    private final ArrayList<Integer> listaFrequentementeAcessada;
    private int[] enderecoLinha;
    private Bloco[] linhas;

    /**
     * Contrutor da Memória Cache
     *
     * @param qtdLinhas quantidade de linhas da cache
     * @param qtdPalavras quantidade de palavras que a linha conterá
     */
    public MemoriaCache(int qtdLinhas, int qtdPalavras) {
        listaRecentementeAcessada = new ArrayList<>();
        listaFrequentementeAcessada = new ArrayList<>();
        this.linhas = new Bloco[qtdLinhas];
        this.enderecoLinha = new int[qtdLinhas];
        for (int cont = 0; cont < linhas.length; cont++) {
            linhas[cont] = new Bloco(qtdPalavras);
        }
        inicializarEnderecoLinhaCache();
    }

    /**
     * Função privada para inicializar os valores da linha cache
     */
    private void inicializarEnderecoLinhaCache() {
        for (int valorLinha = 0; valorLinha < getQtdLinhas(); valorLinha++) {
            enderecoLinha[valorLinha] = valorLinha;

        }
    }

    /**
     * Set de valores na lista de endereços recentemente utilizados
     *
     * @param valorAcesso valores da linha acessada
     */
    public void setListaRecentementeAcessada(int valorAcesso) {
        listaRecentementeAcessada.add(valorAcesso);
    }

    /**
     * Função para remover da lista de endereços recentemente utilizados
     *
     * @param valorAcesso endereço a ser removido
     * @return int valor removido
     */
    public int removeListaRecentementeAcessada(int valorAcesso) {
        return listaRecentementeAcessada.remove(valorAcesso);
    }

    /**
     * Get da lista de endereços recentemente acessados
     *
     * @return ArrayList contendo os endereços acessados
     */
    public ArrayList<Integer> getListaRecentementeAcessada() {
        return listaRecentementeAcessada;
    }

    /**
     * Get da lista de endereços frequentemente acessados
     *
     * @return ArrayList contendo os endereços acessados
     */
    public ArrayList<Integer> getListaFrequentementeAcessada() {
        return listaFrequentementeAcessada;
    }

    /**
     * Set de valores na lista de endereços frequentemente utilizados
     *
     * @param adicionarPosicao valores da linha acessada
     */
    public void setListaFrequentementeAcessada(int adicionarPosicao) {
        listaFrequentementeAcessada.add(adicionarPosicao);
    }

    /**
     * Função para remover da lista de endereços recentemente utilizados
     *
     * @param valorAcesso endereço a ser removido
     * @return int valor removido
     */
    public int removeListaFrequentementeAcessada(int valorAcesso) {
        while (true) {
            try {
                listaFrequentementeAcessada.remove(listaFrequentementeAcessada.indexOf(valorAcesso));
            } catch (Exception ex) {
                break;
            }
        }
        return -1;
    }

    /**
     * Get de quantidade de linhas
     *
     * @return int tamanho da memória cache
     */
    public int getQtdLinhas() {
        return linhas.length;
    }

    /**
     * Get das linhas da cache
     *
     * @return linhas(bloco[]) com as linhas da cache
     */
    public Bloco[] getLinhas() {
        return linhas;
    }

    /**
     * Set de linhas na cache
     *
     * @param linhas Bloco da memoria principal a ser inserido na linha da cache
     */
    public void setLinhas(Bloco[] linhas) {
        this.linhas = linhas;
    }

    /**
     * Get do endereços da linhas da cache
     *
     * @return endereço(int[]) com as linhas da cache
     */
    public int[] getEnderecoLinha() {
        return enderecoLinha;
    }

    /**
     * Set do endereço da linha
     *
     * @param enderecoLinha endereço das linhas a ser inserido
     */
    public void setEnderecoLinha(int[] enderecoLinha) {
        this.enderecoLinha = enderecoLinha;
    }

    /**
     * Função para inserir um bloco da memoria principal na cache
     *
     * @param linha bloco da memoria cache
     * @return boolean se a inserção foi bem sucedida
     */
    public boolean adicionarLinha(Bloco linha) {
        int valorBloco = 0;
        for (Bloco listagem : linhas) {
            if (listagem.getPalavras()[valorBloco].getValorEndereco() == -1) {
                listaRecentementeAcessada.add(valorBloco);
                linhas[valorBloco].setPalavras(linha.getPalavras());
                linhas[valorBloco].setNumeroBloco(linha.getNumeroBloco());
                return true;
            }
            valorBloco++;
        }
        return false;
    }

    /**
     * Função para inserir um bloco da memoria principal na cache
     *
     * @param linha bloco da memoria cache
     * @param enderecoBlocoInstrucao endereço especifico a ser inserido
     * @return boolean se a inserção foi bem sucedida
     */
    public boolean adicionarLinha(Bloco linha, int enderecoBlocoInstrucao) {
        Bloco linhaAdicionada;
        int AdicionarPosicao = linha.getNumeroBloco()[0] % getQtdLinhas();
        if (Fronteira.textResultadoPoliticaEscrita.getText().equals("1")) {
            linhaAdicionada = new Bloco(linha.getQtdPalavras());
            linhaAdicionada.clone(linha);
        } else {
            linhaAdicionada = linha;
        }
        try {
            if (Fronteira.textResultadoMapeamento.getText().equals("2")) {
                AdicionarPosicao = linhaLivre(enderecoBlocoInstrucao);
                listaRecentementeAcessada.add(AdicionarPosicao);
            }
            listaFrequentementeAcessada.add(AdicionarPosicao);
            linhas[AdicionarPosicao].setPalavras(linhaAdicionada.getPalavras());
            linhas[AdicionarPosicao].setNumeroBloco(linhaAdicionada.getNumeroBloco());
            return true;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Endereco Invalido!", "ERRO", JOptionPane.ERROR_MESSAGE);
        }

        return false;
    }

    /**
     * Função não implementada para adição de instrução na memória cache baseada
     * no mapeamento com conjunto
     *
     * @param linha Linha da cache a ser inserida
     * @param endereco valor do endereço a ser inserido
     * @param conjunto conjunto de divisão da memória cache
     * @return Boolean true ou false caso a adição seja bem sucedida
     */
    public boolean adicionarLinha(Bloco linha, int endereco, int conjunto) {
        /*  int limite=linha.getQtdPalavras()/conjunto;
        try {
            //identificar a posição do conjunto dentro da posição normal
            for (int cont = 0; cont < limite; cont++) {
                if (linhas[(endereco % conjunto) + cont].getPalavras()[cont].getValorEndereco() == -1) {
                    linhas[(endereco % conjunto) + cont].setPalavras(linha.getPalavras());
                    linhas[(endereco % conjunto) + cont].setNumeroBloco(linha.getNumeroBloco());
                    return true;
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Endereco Invalido!", "ERRO", JOptionPane.ERROR_MESSAGE);
        }//*/
        return false;
    }

    /**
     * Função para identificar a posição da linha na cache(caso exista)
     *
     * @param bloco bloco a ser pesquisado
     * @return int contendo a posição na cache
     */
    public int identificarPosicaoLinha(Bloco bloco) {
        for (int valorLinha = 0; valorLinha < getQtdLinhas(); valorLinha++) {
            if (bloco.equals(linhas[valorLinha])) {
                return enderecoLinha[valorLinha];
            }
        }

        return 0;
    }

    /**
     * Função para identificar a posição da linha na cache(caso exista)
     *
     * @param bloco bloco a ser pesquisado
     * @param enderecoInstrucao endereço a ser pesquisado
     * @return Bloco contendo o endereço da instrução
     */
    public Bloco identificaLinha(Bloco bloco, int enderecoInstrucao) {
        for (Palavra palavras : bloco.getPalavras()) {
            if (palavras.getValorEndereco() == enderecoInstrucao) {
                if (!Fronteira.textResultadoPoliticaSubstituicao.getText().equals("2") && linhaLivre(enderecoInstrucao) != -1 && !Fronteira.textResultadoMapeamento.getText().equals("1")) {
                    listaRecentementeAcessada.remove(listaRecentementeAcessada.indexOf(linhaLivre(enderecoInstrucao)));
                    listaRecentementeAcessada.add(linhaLivre(enderecoInstrucao));
                }
                return bloco;
            }
        }
        return null;
    }

    /**
     * Função para atribuir o valor no campo de endereço
     *
     * @param linha linha da memória cache a ser manipulada
     * @param enderecoInstrucao endereço a ser inscrito
     * @param valor valor a ser inserido no campo de valores
     */
    public void atribuirValor(Bloco linha, int enderecoInstrucao, int valor) {
        if (linha != null) {
            for (Palavra palavras : linha.getPalavras()) {
                if (palavras.getValorEndereco() == enderecoInstrucao) {
                    palavras.setValorConteudo(valor);
                    break;
                }
            }
        }

    }

    /**
     * Função para verificar se a linha da cache se encontra vazia
     *
     * @param linhaCache linha a ser verificada
     * @param enderecoMemoriaPrincipal endereço da memória a ser manipulada
     * @return boolean caso esteja vazia ou não
     */
    public boolean linhaLivre(Bloco linhaCache, int enderecoMemoriaPrincipal) {
        if (linhaCache == null) {
            return true;
        } else {
            Palavra palavra = linhaCache.getPalavras()[enderecoMemoriaPrincipal % linhaCache.getQtdPalavras()];
            return palavra.getValorEndereco() == -1;
        }
    }

    /**
     * Função para verificar se a linha da cache se encontra vazia
     *
     * @param enderecoInstrucao endereco da memória a ser manipulada
     * @return int posição da memória que se encontra vazia
     */
    public int linhaLivre(int enderecoInstrucao) {
        int cont = 0;
        for (Bloco listagem : linhas) {
            for (Palavra palavras : listagem.getPalavras()) {
                if ((palavras.getValorEndereco() == -1) || (palavras.getValorEndereco() == enderecoInstrucao)) {
                    return cont;
                }
            }
            cont++;
        }
        return -1;
    }

    /**
     * Função para eliminar uma linha da cache
     *
     * @param posicao posição a ser removida
     * @return linha(bloco) removido
     */
    public Bloco eliminarLinha(int posicao) {
        int qtdPalavras = linhas[posicao].getQtdPalavras();
        Bloco eliminado = linhas[posicao];
        try {
            linhas[posicao] = new Bloco(qtdPalavras);
            return eliminado;
        } catch (Exception ex) {
            return eliminado;
        }

    }
    /*
    public void show() {
        System.out.println("Linha - Bloco - Endereco - Conteudo ");
        for (int valorLinha = 0; valorLinha < getQtdLinhas(); valorLinha++) {//8
            for (int valorPalavra = 0; valorPalavra < linhas[0].getQtdPalavras(); valorPalavra++) {//4
                System.out.println(enderecoLinha[valorLinha] + "\t" + linhas[valorLinha].getNumeroBloco()[valorPalavra] + "\t " + linhas[valorLinha].getPalavras()[valorPalavra].getValorEndereco() + "\t\t" + linhas[valorLinha].getPalavras()[valorPalavra].getValorConteudo());
            }
        }

    }
     */
}
