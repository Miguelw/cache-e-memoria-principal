package br.imd.Exception;

/**
 *
 * @author Miguel
 * @version 1.0
 */
public class PoliticaEscritaException extends Exception {

    /**
     * Função que emite a exceção Politica de Escrita
     *
     * @param message mensagem a ser encaminhada para classe pai Exception
     */
    public PoliticaEscritaException(String message) {
        super(message);
    }

    /**
     * Função sobrecarregada contendo a mensagem da exceção
     *
     * @return String Mensagem a ser retornada
     */
    @Override
    public String getMessage() {
        return ("Politica de Escrita incorreta!");
    }
}
