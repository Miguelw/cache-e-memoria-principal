package br.imd.Exception;

/**
 *
 * @author Miguel
 * @version 1.0
 */
public class MapeamentoException extends Exception {

    /**
     * Função que emite a exceção Mapeamento
     *
     * @param message mensagem a ser encaminhada para classe pai Exception
     */
    public MapeamentoException(String message) {
        super(message);
    }

    /**
     * Função sobrecarregada contendo a mensagem da exceção
     *
     * @return String Mensagem a ser retornada
     */
    @Override
    public String getMessage() {
        return ("Mapeamento incorreto!");
    }

}
