package br.imd.Aplicacao;

import br.imd.Controle.Mapeamento;
import br.imd.Dominio.MemoriaCache;
import br.imd.Dominio.MemoriaPrincipal;
import br.imd.Exception.MapeamentoException;
import br.imd.Exception.PoliticaEscritaException;
import br.imd.Exception.PoliticaSubstituicaoException;
import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Miguel
 */
public class Fronteira extends javax.swing.JFrame {

    /**
     * Estruturas Utilizadas
     */
    MemoriaPrincipal memoriaPricipal;
    MemoriaCache memoriaCache;

    //Extensões permitidas    
    FileNameExtensionFilter tipoArquivo = new FileNameExtensionFilter("Txt", "txt");
    //Objeto seletor de arquivo
    JFileChooser fc = new JFileChooser();

    /**
     * Contrutor da classe Fronteira
     */
    public Fronteira() {
        initComponents();
        jLabel6.setVisible(false);
        abrirEstrutura.setVisible(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        radioLeitura = new javax.swing.JRadioButton();
        radioEscrita = new javax.swing.JRadioButton();
        textEndereco = new javax.swing.JTextField();
        textValor = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        textLog = new javax.swing.JTextPane();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        buttonBuscaLeitura = new javax.swing.JButton();
        abrirEstrutura = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jButtonSelecionar = new javax.swing.JButton();
        jLabelArquivo = new javax.swing.JLabel();
        jTextCaminhoArquivo = new javax.swing.JTextField();
        textTituloCapacidadeBloco = new javax.swing.JLabel();
        textTituloLinhasCache = new javax.swing.JLabel();
        textTituloQuantidadeBlocos = new javax.swing.JLabel();
        textTituloMapeamento = new javax.swing.JLabel();
        textTituloQuantidadeConjuntos = new javax.swing.JLabel();
        textTituloPoliticaSubstituicao = new javax.swing.JLabel();
        textTituloPoliticaEscrita = new javax.swing.JLabel();
        textResultadoCapacidadeBloco = new javax.swing.JLabel();
        textResultadoLinhasCache = new javax.swing.JLabel();
        textResultadoQuantidadeBlocos = new javax.swing.JLabel();
        textResultadoMapeamento = new javax.swing.JLabel();
        textResultadoQuantidadeConjuntos = new javax.swing.JLabel();
        textResultadoPoliticaSubstituicao = new javax.swing.JLabel();
        textResultadoPoliticaEscrita = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        CarregarConfiguracoes = new javax.swing.JToggleButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jTabbedPane1MousePressed(evt);
            }
        });

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabel1.setText("Simulador de Memoria Cache");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 30, -1, -1));

        radioLeitura.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        radioLeitura.setSelected(true);
        radioLeitura.setText("Leitura");
        radioLeitura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioLeituraActionPerformed(evt);
            }
        });
        jPanel1.add(radioLeitura, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 110, -1, -1));

        radioEscrita.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        radioEscrita.setText("Escrita");
        radioEscrita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioEscritaActionPerformed(evt);
            }
        });
        jPanel1.add(radioEscrita, new org.netbeans.lib.awtextra.AbsoluteConstraints(333, 108, -1, -1));
        jPanel1.add(textEndereco, new org.netbeans.lib.awtextra.AbsoluteConstraints(177, 161, 117, -1));

        textValor.setEditable(false);
        textValor.setBackground(new java.awt.Color(255, 255, 255));
        textValor.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        textValor.setText("0");
        jPanel1.add(textValor, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 161, 117, -1));

        textLog.setEditable(false);
        jScrollPane1.setViewportView(textLog);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(57, 302, 340, 140));

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Endereço");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 141, -1, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Valor");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 140, -1, -1));

        buttonBuscaLeitura.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        buttonBuscaLeitura.setText("Buscar");
        buttonBuscaLeitura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBuscaLeituraActionPerformed(evt);
            }
        });
        jPanel1.add(buttonBuscaLeitura, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 190, -1, -1));

        abrirEstrutura.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        abrirEstrutura.setText("Visualizar Estruturas");
        abrirEstrutura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                abrirEstruturaActionPerformed(evt);
            }
        });
        jPanel1.add(abrirEstrutura, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 320, -1, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("Log de Operações");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 270, -1, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setText("Estruturas:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 270, -1, -1));

        jTabbedPane1.addTab("Inicial", jPanel1);

        jPanel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel2MouseClicked(evt);
            }
        });
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        jLabel2.setText("Configurações Simulador");
        jPanel2.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(204, 11, -1, -1));

        jButtonSelecionar.setText("Selecionar");
        jButtonSelecionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSelecionarActionPerformed(evt);
            }
        });
        jPanel2.add(jButtonSelecionar, new org.netbeans.lib.awtextra.AbsoluteConstraints(469, 67, -1, -1));

        jLabelArquivo.setText("Caminho do Arquivo");
        jPanel2.add(jLabelArquivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(39, 71, -1, -1));

        jTextCaminhoArquivo.setEditable(false);
        jTextCaminhoArquivo.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.add(jTextCaminhoArquivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(157, 68, 306, -1));

        textTituloCapacidadeBloco.setText("Capacidade do bloco:");
        jPanel2.add(textTituloCapacidadeBloco, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 120, -1, -1));

        textTituloLinhasCache.setText("Linhas da Cache:");
        jPanel2.add(textTituloLinhasCache, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 160, -1, -1));

        textTituloQuantidadeBlocos.setText("Quantidade de blocos:");
        jPanel2.add(textTituloQuantidadeBlocos, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 200, -1, -1));

        textTituloMapeamento.setText("Mapeamento:");
        jPanel2.add(textTituloMapeamento, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 240, -1, -1));

        textTituloQuantidadeConjuntos.setText("Quantidade de Conjuntos:");
        jPanel2.add(textTituloQuantidadeConjuntos, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 270, -1, 18));

        textTituloPoliticaSubstituicao.setText("Politica de Substituição:");
        jPanel2.add(textTituloPoliticaSubstituicao, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 310, -1, -1));

        textTituloPoliticaEscrita.setText("Politica de Escrita:");
        jPanel2.add(textTituloPoliticaEscrita, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 350, -1, -1));

        textResultadoCapacidadeBloco.setText("jLabel9");
        jPanel2.add(textResultadoCapacidadeBloco, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 120, -1, -1));

        textResultadoLinhasCache.setText("jLabel10");
        jPanel2.add(textResultadoLinhasCache, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 160, -1, -1));

        textResultadoQuantidadeBlocos.setText("jLabel11");
        jPanel2.add(textResultadoQuantidadeBlocos, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 200, -1, -1));

        textResultadoMapeamento.setText("jLabel12");
        jPanel2.add(textResultadoMapeamento, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 240, -1, -1));

        textResultadoQuantidadeConjuntos.setText("jLabel13");
        jPanel2.add(textResultadoQuantidadeConjuntos, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 270, 50, 20));

        textResultadoPoliticaSubstituicao.setText("jLabel14");
        jPanel2.add(textResultadoPoliticaSubstituicao, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 310, -1, -1));

        textResultadoPoliticaEscrita.setText("jLabel15");
        jPanel2.add(textResultadoPoliticaEscrita, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 350, -1, -1));

        jButton1.setText("Alterar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 113, -1, -1));

        jButton2.setText("Alterar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 153, -1, -1));

        jButton3.setText("Alterar");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 230, -1, -1));

        jButton4.setText("Alterar");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton4, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 193, -1, -1));

        jButton5.setText("Alterar");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton5, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 270, -1, -1));

        jButton6.setText("Alterar");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton6, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 310, -1, -1));

        jButton7.setText("Alterar");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton7, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 350, -1, -1));

        CarregarConfiguracoes.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        CarregarConfiguracoes.setText("Carregar Configurações");
        CarregarConfiguracoes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CarregarConfiguracoesActionPerformed(evt);
            }
        });
        jPanel2.add(CarregarConfiguracoes, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 410, 220, 30));

        jTabbedPane1.addTab("Configuração", jPanel2);

        getContentPane().add(jTabbedPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 630, 510));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    /**
     * Função para omitir/ocultar visualização inicial das configurações
     */
    private void visualizacao() {
        //Botão de atualização de dados de configuração
        jButton1.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        jButton2.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        jButton3.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        jButton4.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        jButton5.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        jButton6.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        jButton7.setVisible(!jTextCaminhoArquivo.getText().isEmpty());

        //Campo text do título
        textTituloLinhasCache.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        textTituloMapeamento.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        textTituloPoliticaEscrita.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        textTituloPoliticaSubstituicao.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        textTituloQuantidadeBlocos.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        textTituloQuantidadeConjuntos.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        textTituloCapacidadeBloco.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        CarregarConfiguracoes.setVisible(!jTextCaminhoArquivo.getText().isEmpty());

        //Campo text do Resultado
        textResultadoLinhasCache.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        textResultadoMapeamento.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        textResultadoPoliticaEscrita.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        textResultadoPoliticaSubstituicao.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        textResultadoQuantidadeBlocos.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        textResultadoQuantidadeConjuntos.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        textResultadoCapacidadeBloco.setVisible(!jTextCaminhoArquivo.getText().isEmpty());
        CarregarConfiguracoes.setVisible(!jTextCaminhoArquivo.getText().isEmpty());

    }

    /**
     * Função para deixar visivel as configurações
     *
     * @param b1 Arquivo de configuração a ser carregado
     * @throws IOException Exceção de manipulação de arquivo
     */
    public void apresentarConfiguracoes(BufferedReader b1) throws IOException {
        textResultadoCapacidadeBloco.setText(b1.readLine());
        textResultadoLinhasCache.setText(b1.readLine());
        textResultadoQuantidadeBlocos.setText(b1.readLine());
        textResultadoMapeamento.setText(b1.readLine());
        textResultadoQuantidadeConjuntos.setText(b1.readLine());
        textResultadoPoliticaSubstituicao.setText(b1.readLine());
        textResultadoPoliticaEscrita.setText(b1.readLine());
        visualizacao();
    }

    /**
     * Atribui as configurações do arquivo nas estruturas do sistema
     *
     * @param arquivo Arquivo a ser carregado
     */
    public void SetConfiguracoes(File arquivo) {
        try {

            try (FileReader buffer = new FileReader(arquivo)) {

                try (BufferedReader b1 = new BufferedReader(buffer)) {

                    apresentarConfiguracoes(b1);
                    b1.close();

                }

            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Fronteira.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Fronteira.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Identifica o tipo de mapeamento e efetua a leitura ou escrita com base na
     * politica de escrita e sbstituição
     *
     *
     */
    private boolean mapeamento() {
        try {
            if (Integer.parseInt(textEndereco.getText()) < 0) {
                JOptionPane.showMessageDialog(null, "ENDEREÇO INVÁLIDO", "ERRO", JOptionPane.ERROR_MESSAGE);
            } else {

                return Mapeamento.getInstance().selecaoMapeamento(Integer.parseInt(textResultadoMapeamento.getText()), memoriaCache, memoriaPricipal, Integer.parseInt(textEndereco.getText()), Integer.parseInt(textResultadoQuantidadeConjuntos.getText()));
            }
        } catch (MapeamentoException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        } catch (PoliticaEscritaException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        } catch (NumberFormatException | IOException ex) {
            JOptionPane.showMessageDialog(null, "ENDEREÇO INVÁLIDO", "ERRO", JOptionPane.ERROR_MESSAGE);
        } catch (PoliticaSubstituicaoException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "ERRO", JOptionPane.ERROR_MESSAGE);
        }
        return false;
    }

    /**
     * Botão para selecionar o arquivo
     *
     * @param evt evento de click
     */
    private void jButtonSelecionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSelecionarActionPerformed
        //Seleciona os tipos de arquivo com as extensões permitidas
        fc.setFileFilter(tipoArquivo);
        //Titulo da caixa de seleção
        fc.setDialogTitle("Selecionar Arquivo de Configuração");
        //Verifica se selecionou algum arquivo válido
        if (fc.showOpenDialog(null) == 0) {
            File arquivo = new File(fc.getSelectedFile().getAbsolutePath());
            jTextCaminhoArquivo.setText(String.valueOf(arquivo));
            SetConfiguracoes(arquivo);
        }
    }//GEN-LAST:event_jButtonSelecionarActionPerformed

    private void jPanel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseClicked

    }//GEN-LAST:event_jPanel2MouseClicked
    /**
     * Visualização das configurações
     *
     * @param evt eventi de Click
     */
    private void jTabbedPane1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane1MousePressed
        visualizacao();
    }//GEN-LAST:event_jTabbedPane1MousePressed
    /**
     * Alterações do componente
     *
     * @param evt evento de Click
     */
    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        textResultadoQuantidadeConjuntos.setText(JOptionPane.showInputDialog(null, "Informe o valor Correspondente ao campo!", "ALTERAÇÃO DE COMPONENTE", JOptionPane.INFORMATION_MESSAGE));
    }//GEN-LAST:event_jButton5ActionPerformed
    /**
     * Alterações do componente
     *
     * @param evt evento de Click
     */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        textResultadoCapacidadeBloco.setText(JOptionPane.showInputDialog(null, "Informe o valor Correspondente ao campo!", "ALTERAÇÃO DE COMPONENTE", JOptionPane.INFORMATION_MESSAGE));
    }//GEN-LAST:event_jButton1ActionPerformed
    /**
     * Alterações do componente
     *
     * @param evt evento de Click
     */
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        textResultadoLinhasCache.setText(JOptionPane.showInputDialog(null, "Informe o valor Correspondente ao campo!", "ALTERAÇÃO DE COMPONENTE", JOptionPane.INFORMATION_MESSAGE));
    }//GEN-LAST:event_jButton2ActionPerformed
    /**
     * Alterações do componente
     *
     * @param evt evento de Click
     */
    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        textResultadoQuantidadeBlocos.setText(JOptionPane.showInputDialog(null, "Informe o valor Correspondente ao campo!", "ALTERAÇÃO DE COMPONENTE", JOptionPane.INFORMATION_MESSAGE));
    }//GEN-LAST:event_jButton4ActionPerformed
    /**
     * Alterações do componente
     *
     * @param evt evento de Click
     */
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        textResultadoMapeamento.setText(JOptionPane.showInputDialog(null, "Informe o valor Correspondente ao campo!", "ALTERAÇÃO DE COMPONENTE", JOptionPane.INFORMATION_MESSAGE));
    }//GEN-LAST:event_jButton3ActionPerformed
    /**
     * Alterações do componente
     *
     * @param evt evento de Click
     */
    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        textResultadoPoliticaSubstituicao.setText(JOptionPane.showInputDialog(null, "Informe o valor Correspondente ao campo!", "ALTERAÇÃO DE COMPONENTE", JOptionPane.INFORMATION_MESSAGE));
    }//GEN-LAST:event_jButton6ActionPerformed
    /**
     * Alterações do componente
     *
     * @param evt evento de Click
     */
    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        textResultadoPoliticaEscrita.setText(JOptionPane.showInputDialog(null, "Informe o valor Correspondente ao campo!", "ALTERAÇÃO DE COMPONENTE", JOptionPane.INFORMATION_MESSAGE));
    }//GEN-LAST:event_jButton7ActionPerformed
    /**
     * Manipulação dos botões de leitura e escrita
     *
     * @param evt evento de click
     */
    private void radioLeituraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioLeituraActionPerformed
        radioLeitura.setSelected(true);
        radioEscrita.setSelected(false);
        buttonBuscaLeitura.setText("Buscar");
        textValor.setEditable(false);

    }//GEN-LAST:event_radioLeituraActionPerformed
    /**
     * Manipulação dos botões de leitura e escrita
     *
     * @param evt evento de click
     */
    private void radioEscritaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioEscritaActionPerformed
        radioEscrita.setSelected(true);
        radioLeitura.setSelected(false);
        buttonBuscaLeitura.setText("Atribuir");
        textValor.setEditable(true);
    }//GEN-LAST:event_radioEscritaActionPerformed
    /**
     * Função de adição e visualização de instrução
     *
     * @param evt evento de click
     */
    private void buttonBuscaLeituraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBuscaLeituraActionPerformed
        if (CarregarConfiguracoes.isSelected()) {
            if (mapeamento()) {
                textLog.setText(textLog.getText() + "\nMISS IDENTIFICADO\n");
                jLabel6.setVisible(true);
                abrirEstrutura.setVisible(true);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Adicionar Configurações", "ERRO", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_buttonBuscaLeituraActionPerformed
    /**
     * Função para validação das configurações do arquivo txt
     *
     * @param evt evento de click
     */
    private void CarregarConfiguracoesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CarregarConfiguracoesActionPerformed
        try {
            memoriaPricipal = new MemoriaPrincipal(Integer.parseInt(textResultadoCapacidadeBloco.getText()), Integer.parseInt(textResultadoQuantidadeBlocos.getText()));
            memoriaCache = new MemoriaCache(Integer.parseInt(textResultadoLinhasCache.getText()), memoriaPricipal.getBlocos()[0].getQtdPalavras());
            JOptionPane.showMessageDialog(null, "Configuração atribuída corretamente", "AVISO", JOptionPane.INFORMATION_MESSAGE);
            CarregarConfiguracoes.setSelected(true);
            textLog.setText("");
        } catch (NumberFormatException | HeadlessException e) {
            CarregarConfiguracoes.setSelected(false);
            JOptionPane.showMessageDialog(null, "Configuração invalida", "ERRO", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_CarregarConfiguracoesActionPerformed
    /**
     * Função para inicializar o bloco de notas
     *
     * @param evt evento de click
     */
    private void abrirEstruturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_abrirEstruturaActionPerformed
        try {
            Runtime.getRuntime().exec("cmd.exe /c start notepad.exe lib\\log.txt");
        } catch (IOException iOException) {
        }
    }//GEN-LAST:event_abrirEstruturaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Fronteira.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Fronteira.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Fronteira.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Fronteira.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Fronteira().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton CarregarConfiguracoes;
    private javax.swing.JButton abrirEstrutura;
    private javax.swing.JButton buttonBuscaLeitura;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButtonSelecionar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabelArquivo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField jTextCaminhoArquivo;
    public static javax.swing.JRadioButton radioEscrita;
    private javax.swing.JRadioButton radioLeitura;
    private javax.swing.JTextField textEndereco;
    public static javax.swing.JTextPane textLog;
    public static javax.swing.JLabel textResultadoCapacidadeBloco;
    public static javax.swing.JLabel textResultadoLinhasCache;
    public static javax.swing.JLabel textResultadoMapeamento;
    public static javax.swing.JLabel textResultadoPoliticaEscrita;
    public static javax.swing.JLabel textResultadoPoliticaSubstituicao;
    public static javax.swing.JLabel textResultadoQuantidadeBlocos;
    public static javax.swing.JLabel textResultadoQuantidadeConjuntos;
    private javax.swing.JLabel textTituloCapacidadeBloco;
    private javax.swing.JLabel textTituloLinhasCache;
    private javax.swing.JLabel textTituloMapeamento;
    private javax.swing.JLabel textTituloPoliticaEscrita;
    private javax.swing.JLabel textTituloPoliticaSubstituicao;
    private javax.swing.JLabel textTituloQuantidadeBlocos;
    private javax.swing.JLabel textTituloQuantidadeConjuntos;
    public static javax.swing.JTextField textValor;
    // End of variables declaration//GEN-END:variables
}
