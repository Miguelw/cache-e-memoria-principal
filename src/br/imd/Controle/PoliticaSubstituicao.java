package br.imd.Controle;

import br.imd.Aplicacao.Fronteira;
import br.imd.Dominio.MemoriaCache;
import br.imd.Dominio.MemoriaPrincipal;
import br.imd.Exception.PoliticaEscritaException;
import br.imd.Exception.PoliticaSubstituicaoException;
import java.io.IOException;
import java.util.Random;

/**
 *
 * @author Miguel
 * @version 1.0
 */
public abstract class PoliticaSubstituicao {

    /**
     * Contrutor privado da classe de politica de Substituição
     */
    private PoliticaSubstituicao() {
    }

    /**
     * Função de criação estatica para objeto da classe
     *
     * @return Objeto de politica de Substituição
     */
    public static PoliticaSubstituicao getInstance() {
        return new PoliticaSubstituicao() {
        };
    }

    /**
     * Função Menu de seleção da politica de Substituição
     *
     * @param opcao opção de seleção da politica de escrita
     * @param cache Memoria cache a ser manipulada
     * @param memoriaPrincipal Memoria principal a ser manipulada
     * @param endereco endereço da instrução a ser manipulada
     * @return boolean true ou false caso a escrita seja bem sucedida
     * @throws PoliticaEscritaException Emite a exceção de Politica de Escrita
     * @throws IOException Emite a exceção de manipulação de arquivo
     * @throws PoliticaSubstituicaoException Emite a exceção de Politica de
     * Substituição
     */
    public boolean selecaoSubstituicao(int opcao, MemoriaCache cache, MemoriaPrincipal memoriaPrincipal, int endereco) throws PoliticaSubstituicaoException, PoliticaEscritaException, IOException {
        switch (opcao) {
            case 1:
                return substituicaoAleatoria(cache, memoriaPrincipal, endereco);
            case 2:
                return substituicaoFIFO(cache, memoriaPrincipal, endereco);
            case 3:
                return substituicaoLFU(cache, memoriaPrincipal, endereco);

            case 4:
                return substituicaoLRU(cache, memoriaPrincipal, endereco);

            default:
                throw new PoliticaSubstituicaoException("Politica de Substituicao incorreta!");

        }
    }

    /**
     * Função privada de manipulação da substituição aleatória
     *
     * @param cache memoria cache a ser verificada
     * @param memoriaPrincipal memoria principal a ser verificada
     * @param enderecoInstrucao endereço da instrução a ser verificada na cache
     * @return boolean true ou false caso a substituição seja bem sucedida
     * @throws PoliticaEscritaException Emite a exceção de Politica de Escrita
     * @throws IOException Emite a exceção de manipulação de arquivo
     * @throws PoliticaSubstituicaoException Emite a exceção de Politica de
     * Substituição
     */
    private boolean substituicaoAleatoria(MemoriaCache cache, MemoriaPrincipal memoriaPrincipal, int enderecoInstrucao) throws PoliticaEscritaException, IOException, PoliticaSubstituicaoException {
        if (!Fronteira.textResultadoMapeamento.getText().equals("1")) {
            Bloco backup = apagarLinha(cache, new Random().nextInt(cache.getQtdLinhas()));
            backupBloco(memoriaPrincipal, backup);
            return PoliticaEscrita.getInstance().selecaoEscrita(Integer.parseInt(Fronteira.textResultadoPoliticaEscrita.getText()), cache, memoriaPrincipal, enderecoInstrucao);
        }
        return false;
    }

    /**
     * Função privada de manipulação da substituição FIFO
     *
     * @param cache memoria cache a ser verificada
     * @param memoriaPrincipal memoria principal a ser verificada
     * @param enderecoInstrucao endereço da instrução a ser verificada na cache
     * @return boolean true ou false caso a substituição seja bem sucedida
     * @throws PoliticaEscritaException Emite a exceção de Politica de Escrita
     * @throws IOException Emite a exceção de manipulação de arquivo
     * @throws PoliticaSubstituicaoException Emite a exceção de Politica de
     * Substituição
     */
    public boolean substituicaoFIFO(MemoriaCache cache, MemoriaPrincipal memoriaPrincipal, int enderecoInstrucao) throws PoliticaEscritaException, IOException, PoliticaSubstituicaoException {
        if (!Fronteira.textResultadoMapeamento.getText().equals("1")) {
            Bloco backup = cache.eliminarLinha(cache.removeListaRecentementeAcessada(0));
            backupBloco(memoriaPrincipal, backup);
            return PoliticaEscrita.getInstance().selecaoEscrita(Integer.parseInt(Fronteira.textResultadoPoliticaEscrita.getText()), cache, memoriaPrincipal, enderecoInstrucao);
        }
        return false;
    }

    /**
     * Função privada de manipulação da substituição LFU
     *
     * @param cache memoria cache a ser verificada
     * @param memoriaPrincipal memoria principal a ser verificada
     * @param enderecoInstrucao endereço da instrução a ser verificada na cache
     * @return boolean true ou false caso a substituição seja bem sucedida
     * @throws PoliticaEscritaException Emite a exceção de Politica de Escrita
     * @throws IOException Emite a exceção de manipulação de arquivo
     * @throws PoliticaSubstituicaoException Emite a exceção de Politica de
     * Substituição
     */
    private boolean substituicaoLFU(MemoriaCache cache, MemoriaPrincipal memoriaPrincipal, int enderecoInstrucao) throws PoliticaEscritaException, IOException, PoliticaSubstituicaoException {
        if (!Fronteira.textResultadoMapeamento.getText().equals("1")) {
            int qtdLinhas = cache.getQtdLinhas();
            int posicao = cache.getListaFrequentementeAcessada().size();
            int[] linhaMenosUtilizada;
            linhaMenosUtilizada = new int[qtdLinhas];
            for (int cont = 0; cont < qtdLinhas; cont++) {
                linhaMenosUtilizada[cont] = 0;
            }
            qtdLinhas = cache.getListaFrequentementeAcessada().size();
            for (int cont = 0; cont < qtdLinhas; cont++) {
                linhaMenosUtilizada[cache.getListaFrequentementeAcessada().get(cont)]++;
            }
            qtdLinhas = cache.getQtdLinhas();
            int posicaoLinha = 0;
            for (int cont = 0; cont < qtdLinhas; cont++) {
                if (posicao > linhaMenosUtilizada[cont]) {
                    posicao = linhaMenosUtilizada[cont];
                    posicaoLinha = cont;
                }
            }
            cache.removeListaFrequentementeAcessada(posicaoLinha);
            Bloco backup = cache.eliminarLinha(posicaoLinha);
            backupBloco(memoriaPrincipal, backup);
            return PoliticaEscrita.getInstance().selecaoEscrita(Integer.parseInt(Fronteira.textResultadoPoliticaEscrita.getText()), cache, memoriaPrincipal, enderecoInstrucao);
        }
        return false;
    }

    /**
     * Função privada de manipulação da substituição LRU
     *
     * @param cache memoria cache a ser verificada
     * @param memoriaPrincipal memoria principal a ser verificada
     * @param enderecoInstrucao endereço da instrução a ser verificada na cache
     * @return boolean true ou false caso a substituição seja bem sucedida
     * @throws PoliticaEscritaException Emite a exceção de Politica de Escrita
     * @throws IOException Emite a exceção de manipulação de arquivo
     * @throws PoliticaSubstituicaoException Emite a exceção de Politica de
     * Substituição
     */
    private boolean substituicaoLRU(MemoriaCache cache, MemoriaPrincipal memoriaPrincipal, int endereco) throws PoliticaEscritaException, IOException, PoliticaSubstituicaoException {
        if (!Fronteira.textResultadoMapeamento.getText().equals("1")) {
            Bloco backup = cache.eliminarLinha(cache.removeListaRecentementeAcessada(0));
            backupBloco(memoriaPrincipal, backup);
            return PoliticaEscrita.getInstance().selecaoEscrita(Integer.parseInt(Fronteira.textResultadoPoliticaEscrita.getText()), cache, memoriaPrincipal, endereco);
        }
        return false;
    }

    /**
     * Função para apagar a linha da cache
     *
     * @param cache Memoria cache a ser manipulada
     * @param posicao Posição a ser removida da cache
     * @return Linha(Bloco) removido
     */
    private Bloco apagarLinha(MemoriaCache cache, int posicao) {
        return cache.eliminarLinha(posicao);
    }

    /**
     * Função para salvar na memória principal as alterações na memória cache
     *
     * @param memoriaPrincipal Memoria principal a ser manipulada
     * @param linhaCache linha da cache a ser atualizada na memória principal
     */
    public void backupBloco(MemoriaPrincipal memoriaPrincipal, Bloco linhaCache) {
        int posicao = linhaCache.getNumeroBloco()[0];
        memoriaPrincipal.getBlocos()[posicao].clone(linhaCache);
    }

}
