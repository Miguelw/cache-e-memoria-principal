package br.imd.Controle;

import br.imd.Aplicacao.Fronteira;
import br.imd.Dominio.MemoriaCache;
import br.imd.Dominio.MemoriaPrincipal;
import br.imd.Exception.PoliticaEscritaException;
import br.imd.Exception.PoliticaSubstituicaoException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.swing.JOptionPane;

/**
 *
 * @author Miguel
 * @version 1.0
 */
public abstract class PoliticaEscrita {

    private FileWriter arq;
    private PrintWriter gravarArq;

    /**
     * Contrutor privado da classe de politica de Escrita
     */
    private PoliticaEscrita() {
    }

    /**
     * Função de criação estatica para objeto da classe
     *
     * @return Objeto de politica de Escrita
     */
    public static PoliticaEscrita getInstance() {
        return new PoliticaEscrita() {
        };
    }

    /**
     * Função Menu de seleção da politica de Escrita
     *
     * @param opcao opção de seleção da politica de escrita
     * @param cache Memoria cache a ser manipulada
     * @param blocoMemoriaPrincipal Memoria principal a ser manipulada
     * @param endereco endereço da instrução a ser manipulada
     * @return boolean true ou false caso a escrita seja bem sucedida
     * @throws PoliticaEscritaException Emite a exceção de Politica de Escrita
     * @throws IOException Emite a exceção de manipulação de arquivo
     * @throws PoliticaSubstituicaoException Emite a exceção de Politica de
     * Substituição
     */
    public boolean selecaoEscrita(int opcao, MemoriaCache cache, MemoriaPrincipal blocoMemoriaPrincipal, int endereco) throws PoliticaEscritaException, IOException, PoliticaSubstituicaoException {
        switch (opcao) {
            case 1:
                return EscritaWriteBack(cache, blocoMemoriaPrincipal, endereco);
            case 2:
                return EscritaWriteThrough(cache, blocoMemoriaPrincipal, endereco);
            default:
                throw new PoliticaEscritaException("Politica de Escrita incorreta!");

        }
    }

    /**
     * Identifica se a posição da memoria cache está vazia
     *
     * @param cache Memória Cache a ser verificada
     * @param enderecoBloco endereço do bloco contido na memória cache
     * @param enderecoInstrucao endereço da instrução a ser verificada na cache
     * @return Bloco or null bloco contendo o endereço de bloco, ou null caso
     * não esteja na cache
     */
    private Bloco identificaLinha(MemoriaCache cache, int enderecoBloco, int enderecoInstrucao) {
        //Miss
        return cache.identificaLinha(cache.getLinhas()[enderecoBloco % cache.getQtdLinhas()], enderecoInstrucao);
    }

    /**
     * Identifica se ainda há posições livres na cache
     *
     * @param cache cache a ser verificada
     * @return true or false referente se há ou não linha livre
     */
    private int identificaLinha(MemoriaCache cache, int enderecoInstrucao) {
        return cache.linhaLivre(enderecoInstrucao);
    }

    /**
     * Manipulação privada de Escrita no mapeamento direto
     *
     * @param cache Memoria cache a ser manipulada
     * @param enderecoLinhaContemPalavra Endereço da cache que contem a
     * instrução
     * @param enderecoInstrucao Endereço a ser escrito
     * @param valor Valor a ser inscrito na posição da memória
     */
    private void EscritaWriteBack(MemoriaCache cache, int enderecoLinhaContemPalavra, int enderecoInstrucao, int valor) {
        if (Fronteira.radioEscrita.isSelected()) {
            int posicaoMemoriaPrincipal = enderecoLinhaContemPalavra % cache.getQtdLinhas();
            int adicionarPosicao = posicaoMemoriaPrincipal;
            if (Fronteira.textResultadoMapeamento.getText().equals("2")) {
                adicionarPosicao = identificaLinha(cache, enderecoInstrucao);
            }
            if (Fronteira.textValor.getText().isEmpty()) {
                JOptionPane.showMessageDialog(null, "Favor informar o valor a ser atribuído", "ERRO", JOptionPane.ERROR_MESSAGE);
            } else {

                cache.atribuirValor(cache.identificaLinha(cache.getLinhas()[adicionarPosicao], enderecoInstrucao), enderecoInstrucao, valor);
            }
        }
    }

    /**
     * Função privada de tratamento da instrção para escrita direta ou indireta
     * na posição de memória
     *
     * @param cache Memoria cache a ser manipulada
     * @param memoriaPrincipal Memoria Principal a ser manipulada
     * @param enderecoInstrucao Endereço da instrução a ser inserida
     * @return Boolean true ou false caso a escrita seja bem sucedida
     * @throws IOException Emite a exceção de manipulação de arquivo
     * @throws PoliticaSubstituicaoException Emite a exceção de politica
     * substituição
     * @throws PoliticaEscritaException Emite a exceção de politica escrita
     */
    private boolean EscritaWriteBack(MemoriaCache cache, MemoriaPrincipal memoriaPrincipal, int enderecoInstrucao) throws IOException, PoliticaSubstituicaoException, PoliticaEscritaException {
        boolean retorno = false;
        int posicaoMemoriaPrincipal = enderecoInstrucao / memoriaPrincipal.getQtdPalavras();
        int adicionarPosicao = posicaoMemoriaPrincipal;
        if (Fronteira.textResultadoMapeamento.getText().equals("2")) {
            adicionarPosicao = identificaLinha(cache, enderecoInstrucao);
        }
        if (adicionarPosicao != -1) {
            //identifica se a linha não contem o bloco desejado
            try {
                if (identificaLinha(cache, memoriaPrincipal.getBlocos()[adicionarPosicao].getNumeroBloco()[0], enderecoInstrucao) == null) {
                    //identifica se a posicao de meroria a ser inscrita está ocupada
                    if (cache.linhaLivre(cache.getLinhas()[adicionarPosicao % cache.getQtdLinhas()], posicaoMemoriaPrincipal)) {

                        retorno = cache.adicionarLinha(memoriaPrincipal.pegarBloco(enderecoInstrucao), enderecoInstrucao);
                        //retorno true -> bloco da memoria principal atribuído a linha da cache
                    } else {
                        PoliticaSubstituicao.getInstance().backupBloco(memoriaPrincipal, cache.getLinhas()[adicionarPosicao % cache.getQtdLinhas()]);
                        retorno = cache.adicionarLinha(memoriaPrincipal.pegarBloco(enderecoInstrucao), enderecoInstrucao);
                        Fronteira.textLog.setText(Fronteira.textLog.getText() + "\n\nLinha..: "
                                + cache.identificarPosicaoLinha(identificaLinha(cache, memoriaPrincipal.getBlocos()[adicionarPosicao].getNumeroBloco()[0], enderecoInstrucao)) + " atualizada\n");
                    }
                    try {
                        EscritaWriteBack(cache, posicaoMemoriaPrincipal, enderecoInstrucao, Integer.parseInt(Fronteira.textValor.getText()));
                    } catch (Exception ex) {
                        System.out.println("Erro Na escrita");
                    }
                } else {
                    Fronteira.textLog.setText("\n" + Fronteira.textLog.getText() + "\nHIT linha.: "
                            + cache.identificarPosicaoLinha(identificaLinha(cache, memoriaPrincipal.getBlocos()[adicionarPosicao].getNumeroBloco()[0], enderecoInstrucao)));
                    cache.setListaFrequentementeAcessada(cache.identificarPosicaoLinha(identificaLinha(cache, memoriaPrincipal.getBlocos()[adicionarPosicao].getNumeroBloco()[0], enderecoInstrucao)));
                    EscritaWriteBack(cache, posicaoMemoriaPrincipal, enderecoInstrucao, Integer.parseInt(Fronteira.textValor.getText()));
                }
            } catch (ArrayIndexOutOfBoundsException ex) {
                JOptionPane.showMessageDialog(null, "Endereço inválido!", "ERRO", JOptionPane.ERROR_MESSAGE);
            }

        } else {
            return PoliticaSubstituicao.getInstance().selecaoSubstituicao(Integer.parseInt(Fronteira.textResultadoPoliticaSubstituicao.getText()), cache, memoriaPrincipal, enderecoInstrucao);

        }
        salvaLog(cache, memoriaPrincipal);
        return retorno;
    }

    /**
     *
     * Função privada encaminhamento para a escrita Through
     *
     * @param cache Memoria cache a ser manipulada
     * @param memoriaPrincipal Memoria Principal a ser manipulada
     * @param enderecoInstrucao Endereço da instrução a ser inserida
     * @return Boolean true ou false caso a escrita seja bem sucedida
     * @throws IOException Emite a exceção de manipulação de arquivo
     * @throws PoliticaSubstituicaoException Emite a exceção de politica
     * substituição
     * @throws PoliticaEscritaException Emite a exceção de politica escrita
     */
    private boolean EscritaWriteThrough(MemoriaCache cache, MemoriaPrincipal memoriaPrincipal, int endereco) throws IOException, PoliticaSubstituicaoException, PoliticaEscritaException {
        return EscritaWriteBack(cache, memoriaPrincipal, endereco);
    }

    /**
     * Função para salvar o log em arquivo de texto
     *
     * @param cache Memoria cache a ser manipulada
     * @param memoriaPrincipal Memoria Principal a ser manipulada
     * @throws IOException Esceção de manipulação de arquivo
     */
    private void salvaLog(MemoriaCache cache, MemoriaPrincipal memoriaPrincipal) throws IOException {
        this.arq = new FileWriter("lib\\log.txt");
        gravarArq = new PrintWriter(arq);
        int posicaoLinha = 0;

        //Adicionando no log todos os dados da memoria cache
        gravarArq.println("MEMÓRIA CACHE");
        gravarArq.println("Linha - Bloco - Endereco - Conteúdo");
        for (Bloco linhasCache : cache.getLinhas()) {
            for (int cont = 0; cont < linhasCache.getQtdPalavras(); cont++) {
                gravarArq.println(cache.getEnderecoLinha()[posicaoLinha] + "\t" + linhasCache.getNumeroBloco()[cont] + "\t " + linhasCache.getPalavras()[cont].getValorEndereco() + "\t\t" + linhasCache.getPalavras()[cont].getValorConteudo());
            }
            posicaoLinha++;
        }

        //Adicionando no log todos os dados da memoria principal
        int posicaoBloco = 0;
        gravarArq.println();
        gravarArq.println("MEMÓRIA PRINCIPAL");
        gravarArq.println("Bloco - Endereco - Conteúdo");
        for (Bloco blocosMemoria : memoriaPrincipal.getBlocos()) {
            for (int cont = 0; cont < memoriaPrincipal.getBlocos()[0].getQtdPalavras(); cont++) {
                gravarArq.println(memoriaPrincipal.getBlocos()[posicaoBloco].getNumeroBloco()[cont] + "\t" + blocosMemoria.getPalavras()[cont].getValorEndereco() + "\t" + blocosMemoria.getPalavras()[cont].getValorConteudo());
            }
            posicaoBloco++;
        }

        gravarArq.close();
        arq.close();

    }
}
