package br.imd.Controle;

import br.imd.Aplicacao.Fronteira;
import br.imd.Dominio.MemoriaCache;
import br.imd.Dominio.MemoriaPrincipal;
import br.imd.Exception.MapeamentoException;
import br.imd.Exception.PoliticaEscritaException;
import br.imd.Exception.PoliticaSubstituicaoException;
import java.io.IOException;

/**
 *
 * @author Miguel
 * @version 1.0
 */
public abstract class Mapeamento {

    /**
     * Contrutor Privado do mapeamento
     */
    private Mapeamento() {
    }

    /**
     * Implementação statica de retorno da instância da classe Mapeamento
     *
     * @return Mapeamento Objeto derivado da classe
     */
    public static Mapeamento getInstance() {
        return new Mapeamento() {
        };
    }

    /**
     * Função Menu de seleção do mapeamento
     *
     * @param opcao Opção do mapeamento a ser escolhido
     * @param cache Memoria Cahe a ser operada
     * @param MemoriaPrincipal Memoria Principal a ser manipulada
     * @param enderecoInstrucao Endereço da instrução requisitada
     * @param conjunto Conjunto(se necessário) para inserção na memória cache
     * @return Boolean true ou false se o mapeamento foi bem sucedido
     * @throws MapeamentoException Emite a exceção de Mapeamento
     * @throws PoliticaEscritaException Emite a exceção de Politica de Escrita
     * @throws IOException Emite a exceção de manipulação de arquivo
     * @throws PoliticaSubstituicaoException Emite a exceção de Politica de
     * Substituição
     */
    public boolean selecaoMapeamento(int opcao, MemoriaCache cache, MemoriaPrincipal MemoriaPrincipal, int enderecoInstrucao, int conjunto) throws MapeamentoException, PoliticaEscritaException, IOException, PoliticaSubstituicaoException {
        switch (opcao) {
            case 1:
                // n° de blocos da memoria % n° de linhas da cache
                return mapeamentoDireto(cache, MemoriaPrincipal, enderecoInstrucao);
            case 2:

                //1° Mapeamento totalmete associativo sem conjunto(Adiciona no primeiro espaço vago)
                return mapeamentoTotalmenteAssociativo(cache, MemoriaPrincipal, enderecoInstrucao);

            case 3:
                //2° Mapeamento Parcialmente associativo com conjunto(n° de blocos da memoria % # de conjuntos)
                return mapeamentoParcialmenteAssociativo(cache, MemoriaPrincipal, enderecoInstrucao, conjunto);
            default:
                throw new MapeamentoException("Mapeamento Incorreto");
        }
    }

    /**
     * Função privada interna de operação de mapeamento Direto
     *
     * @param cache Memoria Cahe a ser operada
     * @param MemoriaPrincipal Memoria Principal a ser manipulada
     * @param enderecoInstrucao Endereço da instrução requisitada
     * @return Boolean true ou false se o mapeamento foi bem sucedido
     * @throws PoliticaEscritaException Emite a exceção de Politica de Escrita
     * @throws IOException Emite a exceção de manipulação de arquivo
     * @throws PoliticaSubstituicaoException Emite a exceção de Politica de
     * Substituição
     */
    private boolean mapeamentoDireto(MemoriaCache cache, MemoriaPrincipal memoriaPrincipal, int enderecoInstrucao) throws PoliticaEscritaException, IOException, PoliticaSubstituicaoException {

        return PoliticaEscrita.getInstance().selecaoEscrita(Integer.parseInt(Fronteira.textResultadoPoliticaEscrita.getText()), cache, memoriaPrincipal, enderecoInstrucao);

    }

    /**
     * Função privada interna de operação de mapeamento Totalmente Associativo
     *
     * @param cache Memoria Cahe a ser operada
     * @param MemoriaPrincipal Memoria Principal a ser manipulada
     * @param enderecoInstrucao Endereço da instrução requisitada
     * @return Boolean true ou false se o mapeamento foi bem sucedido
     * @throws PoliticaEscritaException Emite a exceção de Politica de Escrita
     * @throws IOException Emite a exceção de manipulação de arquivo
     * @throws PoliticaSubstituicaoException Emite a exceção de Politica de
     * Substituição
     */
    private boolean mapeamentoTotalmenteAssociativo(MemoriaCache cache, MemoriaPrincipal memoriaPrincipal, int enderecoInstrucao) throws PoliticaEscritaException, IOException, PoliticaSubstituicaoException {
        return PoliticaEscrita.getInstance().selecaoEscrita(Integer.parseInt(Fronteira.textResultadoPoliticaEscrita.getText()), cache, memoriaPrincipal, enderecoInstrucao);
    }

    /**
     * Função privada interna de operação de mapeamento Totalmente Associativo
     *
     * @param cache Memoria Cahe a ser operada
     * @param MemoriaPrincipal Memoria Principal a ser manipulada
     * @param enderecoInstrucao Endereço da instrução requisitada
     * @return Boolean true ou false se o mapeamento foi bem sucedido
     * @throws PoliticaEscritaException Emite a exceção de Politica de Escrita
     * @throws IOException Emite a exceção de manipulação de arquivo
     * @throws PoliticaSubstituicaoException Emite a exceção de Politica de
     * Substituição
     */
    private boolean mapeamentoParcialmenteAssociativo(MemoriaCache cache, MemoriaPrincipal blocoMemoriaPrincipal, int endereco, int conjunto) {
        return cache.adicionarLinha(blocoMemoriaPrincipal.pegarBloco(endereco), endereco, conjunto);
    }
}
