package br.imd.Controle;

/**
 *
 * @author Miguel
 * @version 0.2
 */
public class Bloco {

    private int[] numeroBloco;
    private Palavra[] palavras;

    /**
     * Construtor da Classe Bloco
     *
     * @param qtdPalavras int quantidade de palavras que o bloco conterá
     */
    public Bloco(int qtdPalavras) {
        this.palavras = new Palavra[qtdPalavras];
        this.numeroBloco = new int[qtdPalavras];
        for (int cont = 0; cont < palavras.length; cont++) {
            palavras[cont] = new Palavra(-1, -1);
            numeroBloco[cont] = -1;
        }
    }

    /**
     * get Valor da posicao do bloco
     *
     * @return int[] vetor contendo todos os valores de posições do bloco
     */
    public int[] getNumeroBloco() {
        return numeroBloco;
    }

    /**
     * set Valor a ser inserido na posicao do bloco
     *
     * @param numeroBloco vetor de int a ser atribuído na numeração dos blocos
     */
    public void setNumeroBloco(int[] numeroBloco) {
        this.numeroBloco = numeroBloco;
    }

    /**
     * get Quantidade de Palavras
     *
     * @return int com a capacidade de palavras no bloco
     */
    public int getQtdPalavras() {
        return palavras.length;
    }

    /**
     * get Vetor contendo todas as palavras
     *
     * @return Palavras[], retorna um vetor contendo todas as palavras
     */
    public Palavra[] getPalavras() {
        return palavras;
    }

    /**
     * Set Palavras, insere um vetor de palavras
     *
     * @param palavras Palavra[] a ser adicionada no bloco
     */
    public void setPalavras(Palavra[] palavras) {
        this.palavras = palavras;
    }

    /**
     * Inserir Endereço no bloco
     *
     * @param posicao posição a ser inserido o valor
     * @param valorLinha valor a ser adicionado na linha
     */
    public void inserirEnderecoBloco(int posicao, int valorLinha) {
        numeroBloco[posicao] = valorLinha;
    }

    /**
     * Clone de Blocos
     *
     * @param linha bloco a ser clonado
     */
    public void clone(Bloco linha) {
        int endereco, valor, cont = 0;
        for (Palavra listaPalavra : palavras) {
            endereco = linha.getPalavras()[cont].getValorEndereco();
            valor = linha.getPalavras()[cont].getValorConteudo();
            listaPalavra.setValorConteudo(valor);
            listaPalavra.setValorEndereco(endereco);
            cont++;
        }
        int[] numerosBloco;
        numerosBloco = linha.getNumeroBloco();
        this.numeroBloco = numerosBloco;
    }

}
