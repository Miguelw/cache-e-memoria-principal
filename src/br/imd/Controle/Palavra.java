package br.imd.Controle;


/**
 *
 * @author Miguel
 * @version 0.2
 */
public class Palavra {
    
    private int valorEndereco;
    private int valorConteudo;

    /**
     *
     * Construtor da classe Palavra
     *
     * @param valorEndereco valor referente ao endereço das palavras
     * @param valorConteudo valor referente ao conteúdo das palavras
     */
    public Palavra(int valorEndereco, int valorConteudo) {
        this.valorEndereco = valorEndereco;
        this.valorConteudo = valorConteudo;
    }

    /**
     * get do valor Endereço
     *
     * @return int Valor referente ao endereço da palavra
     */
    public int getValorEndereco() {
        return valorEndereco;
    }

    /**
     * set do valor Endereço
     *
     * @param valorEndereco Valor a ser atribuído
     */
    public void setValorEndereco(int valorEndereco) {
        this.valorEndereco = valorEndereco;
    }

    /**
     * get do valor Conteúdo
     *
     * @return int Valor referente ao Conteúdo da palavra
     */
    public int getValorConteudo() {
        return valorConteudo;
    }

    /**
     * set do valor Conteudo
     *
     * @param valorConteudo Valor a ser atribuído
     */
    public void setValorConteudo(int valorConteudo) {
        this.valorConteudo = valorConteudo;
    }

}
